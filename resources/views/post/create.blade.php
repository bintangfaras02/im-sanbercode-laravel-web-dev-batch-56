@extends('layout.master')

@section('judul')
    HALAMAN GENRE
    @endsection

    @section('content')

    <form action="/post" method="post" enctype="multipart/formdata">
    
       
      @csrf
  <div class="form-group">
    <label >Judul</label>
    <input type="text" name="judul" class="form-control" >
  </div>
  @error('judul')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror

  <div class="form-group">
    <label >genre</label>
    <textarea name="genre" class="form-control" cols="30" rows="10"></textarea>
</div>
@error('genre')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label >film</label>
    <input type="file" name="film" class="form-control" >
  </div>
  @error('film')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror
 
  <div class="form-group">
    <label >kategori</label>
    <select name="kategori_id" class="form-control" id="">
    <option value= "" >--pilih kategori--</option>
     @forelse ($kategori as $item)
          <option value="{{$item->id}}"> {{$item->nama}} </option>
     @empty
     <option value= "" >tidak ada data</option>
 @endforelse
</select>
  
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection