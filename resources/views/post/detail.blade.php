@extends('layout.master')

@section
Halaman detail Post
@endsection

@section('content')

<div class="card">
<img src="{{asset('image/' . $post->film)}}" class="card-img-top" alt="...">
<div class="card-body">
    <h3>({{$post->judul}})</h3>
    <p class="card-text">{{$post->content}} </p>
    <a href="/post" class="btn btn-secondary btn-block btn-sm">kembali</a>
   

    
</div>
</div>

@endsection