@extends('layout.master')

@section('judul')
    TAMPILAN
    @endsection 

    @section('content')



<a href="/posts/create" class="btn btn-primary">Tambah</a>
       
            <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">nama</th>
      <th scope="col">action</th>
   
    </tr>
  </thead>
  <tbody>
    @forelse ($kategori as $key => $item)
    <tr>
      <th scope="row">{{$key + 1}}</th>
      <td>{{$item->nama}}</td>
     

      <td>
        <form action="/kategori/{{$item->id}}" method="POST">
          @csrf
<a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
<a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
@method('DELETE')
<input type="submit" value="Delete" class="btn btn-danger btn-sm">
</td>
      
</tr>
@empty
    <p>No users</p>
@endforelse
  </tbody>
</table>
@endsection
