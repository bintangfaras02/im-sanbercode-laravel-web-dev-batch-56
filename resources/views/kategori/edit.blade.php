@extends('layout.master')

@section('judul')
    HALAMAN JUDUL
    @endsection 

    @section('content')

    <form action="/kategori/{{$kategori->id}}" method="POST">

    
      @csrf
      @method('PUT')
  <div class="form-group">
    <label >Nama kategori</label>
    <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control" >
  </div>
  <div class="form-group">
    <label >Deskripsi kategori</label>
    <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
   
  
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
