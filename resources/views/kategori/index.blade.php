@extends('layout.master')

@section('judul')
    HALAMAN JUDUL
    @endsection 

    @section('content')

    <form method="post" action="/kategori">
    
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
      @csrf
  <div class="form-group">
    <label >Nama kategori</label>
    <input type="text" name="nama" class="form-control" >
  </div>
  <div class="form-group">
    <label >Deskripsi kategori</label>
    <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
   
  
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
