<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;
use App\Http\Controllers\genrecontroller;

Route::get('/ ', [HomeController::class, 'utama']);

Route::get('/register', [AuthController::class, 'daftar']);



Route::get('/data-Table', function() {
    return view('page.dataTable');
});

//CRUD CAST
Route::get('/kategori/create', [castController::class, 'create']);
Route::post('/kategori', [castController::class, 'store']);
Route::get('/kategori', [castController::class, 'index']);
Route::get('/kategori/{kategori_id}',[castController::class, 'show']);
Route::get('/kategori/{kategori_id}/edit',[castController::class, 'edit']);
Route::put('/kategori/{kategori_id}',[castController::class, 'update']);
Route::delete('/kategori/{kategori_id}',[castController::class, 'destroy']);

Route::resource('post', genrecontroller::class);





