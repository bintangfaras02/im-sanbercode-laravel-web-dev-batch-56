<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori;
use App\Models\post;

class genrecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $post = kategori::get();
        return view('post.index',['post' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
        $kategori = kategori::get();
        return view('post.create',['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'judul' => 'required|min:6',
            'genre' => 'required',
            'kategori_id' => 'required',
            'film' => 'required|image|mimes:jpg,png,jpeg|max:2048'
           
        ]);
        $filmName = time().'.'.$request->film->extension();
        $request->film->move(public_path('image'), $filmName);

        $post = new post;

        $post->judul= $request->judul;
        $post->genre = $request->genre;
        $post->kategori_id= $request->kategori_id;
        $post->film= $postName;

        $post->save();
        
        return redirect('/post');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = kategori::find($id);

        return view('post.detail', ['post' =>$post]);
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
