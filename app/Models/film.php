<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fil extends Model
{
    use HasFactory;

    protected $table='post';

    protected $fillable = ['judul','genre','film','kategori_id'];
}
